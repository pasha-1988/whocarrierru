<!DOCTYPE html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">
    <title>Узнать оператора по номеру телефона, кто оператор</title>
    <meta name="yandex-verification" content="3232488f5b1d549f" />

    <meta name="keywords" content="узнать оператора, кто опреатор, определить оператора по телефону, кто мой оператор" />
    <!-- Bootstrap CSS -->
    <link rel="manifest" href="/manifest.json">
    <link rel="stylesheet" href="/css/app.css">
    <meta name="theme-color" content="#ffffff">

</head>
<body>

<div class="container">
    <div id="app">
        <div class="col-12-xs">
            <p class="lead">
                Сервис поможет узнать оператора, по номеру телефона, обновление данных происходит раз в неделю.
            </p>
        </div>
        <div class="col-12-xs">
            <label for="searchPhoneNumber">Номер телефона</label>
            <div class="input-group">

                <input v-on:keyup.enter="sendNumber" v-model.trim="inputNumber" id="searchPhoneNumber" class="form-control form-control-primary"
                       placeholder="8900123456">
                <div @click="sendNumber" class="input-group-addon button-search">
                    <img alt="" class="search-icon" src="/icons/magnifying-glass.svg">
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/js/app.js"></script>
</body>
</html>


