/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
const VueResource = require('vue-resource');
Vue.component('result-info', require('./components/Result-info'));
Vue.use(VueResource);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const template =`
  <div>
        <div class="col-12-xs">
            <p class="lead">
                Сервис поможет узнать оператора, по номеру телефона, обновление данных происходит раз в неделю.
            </p>
        </div>
        <div class="col-12-xs">
            <label for="searchPhoneNumber">Номер телефона</label>
            <div class="input-group">

                <input v-on:keyup.enter="sendNumber" v-model.trim="inputNumber" id="searchPhoneNumber" class="form-control form-control-primary"
                       placeholder="8900123456">
                <div @click="sendNumber" class="input-group-addon button-search">
                    <img alt="" class="search-icon" src="/icons/magnifying-glass.svg">
                </div>
            </div>
        </div>
        <div class="col-12-xs">
            <div class="card border-danger card-result mb-3" v-show="showNotFoundBlock">
                <div class="card-body text-danger">
                    <p class="card-text">По вашему запросу ничего не найдено.</p>
                </div>
            </div>
            <result-info v-for="item in resultInformation" :carrier-name="item.carrierName"
                         :country-area="item.countryArea">
            </result-info>
        </div>
    </div>
`;
const app = new Vue({
    el: '#app',
    template,
    data: {

        inputNumber: '',
        resultInformation: [],
        showNotFoundBlock: false
    },
    methods: {

        sendNumber() {

            this.$http.get('/phonenumbers/' + this.formatNumber).then(response => {

                this.resultInformation = response.body;
                if (response.body.length === 0) {

                    this.showNotFoundBlock = true;
                } else {
                    this.showNotFoundBlock = false;
                }

            }, response => {

            });
        }

    },
    computed: {

        formatNumber() {

            let inputNumberClean = this.inputNumber.replace(/[^\d]/g, "");
            let firstCharset = inputNumberClean.substring(0, 1);
            if (firstCharset === '8') {

                return inputNumberClean.substring(1)
            }

            if (firstCharset === '7') {

                return inputNumberClean.substring(1)
            }

            return inputNumberClean;
        }
    },
    mounted:function () {


    }
});
if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js').then(function(registration) {
        // Registration was successful
    }).catch(function(err) {
        // registration failed :(
        console.log('ServiceWorker registration failed: ', err);
    });
}

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-105826138-1', 'auto');
ga('send', 'pageview');

