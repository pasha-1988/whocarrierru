<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rus', function (Blueprint $table) {

            $table->increments('id');
            $table->text('carrierName')->nullable();
            $table->text('countryArea')->nullable();
            $table->bigInteger('rangeStart');
            $table->bigInteger('rangeEnd');
//            $table->timestamps();
            $table->index(['rangeStart', 'rangeEnd']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rus');
    }
}
