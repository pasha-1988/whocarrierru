<?php

namespace App;

use Storage;

class RuPhones
{

    private $pageLink = 'https://www.rossvyaz.ru/activity/num_resurs/registerNum/';
    private $doamin = 'https://www.rossvyaz.ru/';

    public function getInfoPhones(): \Illuminate\Support\Collection
    {

        $page = $this->getPageWithLink();
        $links = $this->parsePage($page);
        $files = $this->downloadFiles($links);
        $rows = $this->parseFiles($files);
        $numbersInfo = $this->parseRows($rows);
        $numbersInfo = $this->formatting($numbersInfo);
        $numbersInfo = $this->convertEncoding($numbersInfo);

        return $numbersInfo;
    }

    private function getPageWithLink(): string
    {

        $httpClient = new \HttpHelper\Client();
        $httpClient->setUrl($this->pageLink);
        $httpClient->run();

        return $httpClient->responseBody;

    }

    private function parsePage(string $page): \Illuminate\Support\Collection
    {

        $re = '/a\shref\=\"([^\s]*\.csv)\"/m';
        preg_match_all($re, $page, $matches, PREG_SET_ORDER, 0);

        $results = collect();
        foreach ($matches as $math) {

            $results->push($math[1]);
        }

        return $results;

    }


    private function downloadFiles(\Illuminate\Support\Collection $links): \Illuminate\Support\Collection
    {

        $result = collect();
        foreach ($links as $link) {

            $httpClient = new \HttpHelper\Client();
            $httpClient->setUrl($this->doamin . $link);
            $httpClient->run();
            $tmpFilename = str_random(10);
            Storage::disk('local')->put($tmpFilename, $httpClient->responseBody);
            $result->push($tmpFilename);

        }

        return $result;

    }

    private function parseFiles(\Illuminate\Support\Collection $files): array
    {
        $result = [];

        foreach ($files as $file) {

            $content = Storage::disk('local')->get($file);
            $rows = $this->explodeContentOnRows($content);
            $this->deleteTitleInFile($rows);
            $result = array_merge($result, $rows);

        }

        return $result;
    }

    private function explodeContentOnRows(string $content): array
    {
        $result = explode("\n", $content);
        return $result;
    }

    private function deleteTitleInFile(array &$contentFile)
    {
        array_shift($contentFile);
    }

    private function checkParseRow(array $explodeRow): bool
    {


        if (sizeof($explodeRow) !== 6) {

            return false;
        }

        return true;
    }

    private function parseRows(array $rows): \Illuminate\Support\Collection
    {

        $result = collect();

        foreach ($rows as $row) {

            $explodeRow = explode(";", $row);

            $validateRow = $this->checkParseRow($explodeRow);

            if (!$validateRow) {

                continue;
            }
            $infoRange = (object)[];
            $infoRange->phonePrefix = $explodeRow[0];
            $infoRange->rangeStart = $explodeRow[1];
            $infoRange->rangeEnd = $explodeRow[2];
            $infoRange->carrierName = $explodeRow[4];
            $infoRange->countryArea = $explodeRow[5];
            $result->push($infoRange);

        }

        return $result;

    }

    private function formatting(\Illuminate\Support\Collection $numbersInfo): \Illuminate\Support\Collection
    {

        foreach ($numbersInfo as &$info) {

            foreach ($info as $key => $value) {

                $info->$key = trim($value);
            }

//            $info->phonePrefix = intval($info->phonePrefix);
//            $info->rangeStart = intval($info->rangeStart);
//            $info->rangeEnd = intval($info->rangeEnd);

        }

        return $numbersInfo;

    }

    private function convertEncoding(\Illuminate\Support\Collection $numbersInfo): \Illuminate\Support\Collection
    {
        foreach ($numbersInfo as &$info) {
            $info->carrierName = mb_convert_encoding($info->carrierName, "utf-8", "windows-1251");
            $info->countryArea = mb_convert_encoding($info->countryArea, "utf-8", "windows-1251");

        }

        return $numbersInfo;
    }
}