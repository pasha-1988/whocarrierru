<?php

namespace App\Http\Controllers;

use App\RuPhones;
use Illuminate\Http\Request;
use App\Ru;
use DB;

class LoadPhones extends Controller
{

    public function start()
    {

        $ruPhones = new RuPhones();
        $numbers = $ruPhones->getInfoPhones();

        Ru::truncate();
        foreach ($numbers as $number) {

            $info = new Ru();
            $info->carrierName = $number->carrierName;
            $info->countryArea = $number->countryArea;
            $info->rangeStart = $number->phonePrefix . $number->rangeStart;
            $info->rangeEnd = $number->phonePrefix . $number->rangeEnd;
            $info->countryArea = $number->countryArea;
            $info->save();
        }

    }
}
